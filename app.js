var bcrypt = require('bcrypt');
const SRP6JavascriptClientSessionSHA512 = require('./SRP6JavascriptClientSessionSHA512');

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());

app.post('/tyroChallenge', (req, res) => {
    let password = req.body.password;
    let challenges = req.body.challenges;

    let credential = '';

    challenges.forEach(challenge => {
        var srpClient = new SRP6JavascriptClientSessionSHA512();
        var stretchedPassword = bcrypt.hashSync(password, challenge.salt);
        srpClient.step1(challenge.challengeId, stretchedPassword);
        var credentials = srpClient.step2(challenge.salt, challenge.serverB);
        if (credential.length !== 0) {
            credential += "||";
        }
        credential += credentials.M1 + "||" + credentials.A;
    })

    res.send(credential);
})

var port = process.env.PORT || 3000

app.listen(port);